#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/
#+REVEAL_THEME: solarized 
#+REVEAL_TRANS: linear
#+OPTIONS: num:nil toc:nil

#+Title: The fathom protocol
#+Author: Jared Pereira
#+Email: jared.pereira@consensys.net

* What do we want? 
  A single source of truth for credentials
** Why is this important?
   Individuals shape their learning based on what they can reliably communicate
   to others.
   
   /Trusted/ credentials reduce transaction costs for coordinating
* What is it?
  *A sharded proof-of-stake system for assessing knowledge*
  
  There is a token. We call it AHA
** Motivations:
   - We want the reputation of the credential issuance system to be derived from
     the consensus mechanism, not from the reputation of a central operator.
   - We want the payout from participating in the consensus system to depend on
     the "value" of the network.
* Basic structure
** Concepts
  The network is organized via *concepts*
  
  Each concept can have 1 or more *parents* which are concepts more general than
  it.
  
  Anyone can create a new concept
** Assessments
   To join a concept you have to go through an assessment
   
   Members of the concept and of parent concepts are the ones who will assess
   you.
** Motivations
   - We want the network to grow naturally. A new concept points to older
     established ones their starting point.
   - We want later concept to add to the reputation
   - The community should converge on concepts
* Starting an Assessment
  1. Anyone can start an assessment by paying X AHA per Y assessors
  2. A subset of assessors eligible (members of concept + parents) are
     "randomly" called
  3. They have the opporunity to stake X AHA to accept the assessment, 
  4. once Y assessors  have staked the assessment begins
** Motivations
   - Assessors shouldn't _have_ to assess as assessing may require specialized
     knowledge they don't have
   - Only the most informed subset of the total assessor pool is eligible to assess
* Running an assessment
  - No globally defined assessment mechanism
  - scores are -127 to 127
  - commit/reveal/steal for submitting scores
** Motivations
   It should be difficult as possible for assessors to coordinate out-of-channel
* Calculating the final score
  #+CAPTION: Calculting the consensus score
 [[./assets/fathom%20infographics%208:25:18/how%20final%20score%20is%20calculated/final%20score.gif]]
** Motivations
   We should be able to disregard assessors out of consensus 
* Redistributing tokens!
  #+CAPTION: How assessors get paid out
  [[./assets/fathom%20infographics%208:25:18/how%20assessors%20are%20paid/payment%202.png]]
** Notes
   - A portion of the tokens burned from the losing cluster is redistributed to
     those in the winning cluster!
* Weight in Concepts
  If you're positively assessed you earn a weight in the concept
  
  weight = final score *  assessors in the final cluster

  higher weight means higher likelihood of being called for assessments.
* Token Inflation
  - New tokens are minted every epoch
  - At the start of an epoch the blockhash is set as the target hash
  - Assessors who have staked tokens during the epoch can submit bids
  
  =Bid = keccack(1 - tokens_staked, 1 - time_staked, assessment address,
  assessor address)=
  
  - The closest hash to the target hash wins the new tokens.
* Future extensions
** Concept Cloning
   You can create a clone of concepts, which current members can opt in to
   transfering to. 
** Concept Tax
   We require individuals to stake to create concepts and they can collect tax
   from assessments created in that concept
